Modern fork of the Symmetrica library from the SageMath team.


About
=====
This is a fork of symmetrica-2.0, maintained by the SageMath
development team. The original developers of symmetrica at the
University of Bayreuth are no longer active, but symmetrica is still
in use in SageMath and elsewhere. This fork was created to modernize
the codebase, and to resume making releases with the fixes that have
accrued over the years.


Building a release
==================
If you are building from the release tarball, the standard

```
./configure
make
make install
```

should suffice to build and install the library. Afterwards, the test
suite can be run with

```
make check
```

to ensure that everything went as planned. In addition to the standard
`./configure` flags controlling the installation directories, the
following flags are also supported:

  * `--enable-doc`: install the API documentation


Development
===========
The new home for Symmetrica development is on Gitlab:

  https://gitlab.com/sagemath/symmetrica

Bugs and feature requests should be reported there from now on. To
bootstrap the autotools build system within the git repository, first
run

```
autoreconf -fi
```

after which the standard instructions apply.
