AC_INIT(
  [SYMMETRICA],
  [2.0.1],
  [https://gitlab.com/sagemath/symmetrica/issues],
  [symmetrica],
  [https://gitlab.com/sagemath/symmetrica])

AM_INIT_AUTOMAKE([-Wall foreign no-dist-gzip dist-xz tar-ustar])
AC_CONFIG_MACRO_DIR([m4])

dnl Any old source file suffices here; it's a canary.
AC_CONFIG_SRCDIR([src/galois.c])

dnl This custom macro will check if the linker supports version scripts
dnl (like our src/symmetrica.map), and define HAVE_LD_VERSION_SCRIPT
dnl accordingly.
gl_LD_VERSION_SCRIPT

dnl Check for a C99 compiler and archiver.
AC_PROG_CC_C99
AM_PROG_AR

dnl Since we now presumably have a C compiler, let's see if it
dnl supports the -Wno-unused-result flag. The library consistently
dnl ignores the result of things like scanf, and GCC (at least)
dnl complains about it. So we'd like to disable those warnings,
dnl but we don't want to pass an unknown flag to a non-GCC compiler!
AX_CHECK_COMPILE_FLAG([-Wno-unused-result],
                      [AM_CONDITIONAL([HAVE_WNO_UNUSED_RESULT],[true])])

dnl Ensure that "libm" refers to the math library on this machine when
dnl we go to link against it.
LT_LIB_M

dnl Check for all of the standard headers that we use.
AC_CHECK_HEADERS([ inttypes.h malloc.h memory.h stdlib.h ])
AC_CHECK_HEADERS([ string.h sys/param.h unistd.h ])

dnl Define int32_t, uint32_t, and size_t to be suitable types if they're
dnl not defined already and if a suitable type exists on the system. See
dnl the autoconf documentation if you're actually worried about this.
AC_TYPE_INT32_T
AC_TYPE_UINT32_T
AC_TYPE_SIZE_T

dnl Check for some specific library functions that we call.
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_CHECK_FUNCS([memset])

AC_ARG_ENABLE([doc],
  AS_HELP_STRING([--enable-doc], [install the API documentation]),
  [enabledoc=$enableval],
  [enabledoc=no]
)
AM_CONDITIONAL([ENABLEDOC], [test x$enabledoc = xyes])

AC_CONFIG_FILES([ Makefile doc/Makefile src/Makefile ])
AC_CONFIG_FILES([ src/run-tests.sh ], [ chmod +x src/run-tests.sh ])

LT_INIT
AC_OUTPUT
